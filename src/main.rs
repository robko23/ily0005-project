use proj::sum;

fn main() {
    let a = 1;
    let b = 2;
    let res = sum(1,2);
    println!("Sum of {a} and {b} = {res}");
}
