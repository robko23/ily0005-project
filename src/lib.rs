pub fn sum(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use crate::sum;

    #[test]
    fn test_sum() {
        assert_eq!(sum(1, 1), 2);
        assert_eq!(sum(3, 2), 5);
    }
}
