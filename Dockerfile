#syntax=docker/dockerfile:1.4

# args
ARG RUST_BASE_IMAGE_VERSION=1.77.0
ARG RUST_BASE_IMAGE_REPO=docker.io/blackdex/rust-musl:x86_64-musl-stable-${RUST_BASE_IMAGE_VERSION}
ARG RUN_IMAGE=gcr.io/distroless/static-debian11:nonroot-amd64

FROM ${RUST_BASE_IMAGE_REPO} as builder

WORKDIR /app

COPY . .

RUN cargo build --release --bin proj --target x86_64-unknown-linux-musl

FROM ${RUN_IMAGE}
WORKDIR /app
COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/proj .
ENTRYPOINT [ "/app/proj" ]
